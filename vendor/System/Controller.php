<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace System;

/**
 * Description of Controller
 *
 * @author youssef
 */
abstract class Controller {
    
    protected $app;
    
    public function __construct(Application $app) {
        $this->app=$app;
    }
    
    public function __get($key) {
        return $this->app->get($key);
    }
}
