<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace System\Http;

/**
 * Description of Request
 *
 * @author youssef
 */
class Request {
    private $url;
    private $baseURL;
    public function prepareURL(){
        $script=  dirname($this->server('SCRIPT_NAME'));
        $requestURI=  $this->server('REQUEST_URI');
        if(strpos($requestURI,'?')!==FALSE){
            list($requestURI,$qString)=  explode('?', $requestURI);
        }
        $this->url=  preg_replace('#^'.$script.'#', '', $requestURI);
        $this->baseURL=  $this->server('REQUEST_SCHEME').'://'.$this->server('HTTP_HOST').$script.'/';
       // echo $this->baseURL;
       /* 
        echo '<pre>';
        print_r($_SERVER);
        echo '</pre>';*/
    }
    public function server($key,$default=null){
        return isset($_SERVER[$key])?$_SERVER[$key]:$default;
    }
    public function url(){
        return $this->url;
    }
    
    public function post($key,$default=null){
        return isset($_POST[$key])?$_POST[$key]:$default;
    }
    
    public function get($key,$default=null){
        return isset($_GET[$key])?$_GET[$key]:$default;
    }
    
    public function method(){
        return $this->server('REQUEST_METHOD');
    }
    public function baseURL(){
        return $this->baseURL;
    }
    
}
