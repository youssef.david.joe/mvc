<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace System\Http;
use System\Application;
/**
 * Description of Response
 *
 * @author youssef
 */
class Response {
   
    private $app;
    
    private $headers=[];
    
    private $content='';
    
    public function __construct(Application $app) {
        $this->app=$app;
    }
    
    public function setOutput($content){
        $this->content=$content;
    }
    
    
    public function setHeaders($header,$value){
        $this->headers[$header]=$value;
    }
     
    public function send(){
        $this->sendHeaders();
        $this->sendOutput();
    }
    
    private function sendHeaders(){
        foreach ($this->headers as $header =>$value){
            header($header.':'.$value);
        }
        
    }
    
    private function sendOutput(){
        echo $this->content;
    }
    
}
