<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace System\View;

/**
 * Description of ViewInterface
 *
 * @author youssef
 */
interface ViewInterface {
   
    public function getOutput();
    public function __toString();
}
