<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace System\View;

use System\Application;
/**
 * Description of ViewFactory
 *
 * @author youssef
 */
class ViewFactory {
    
    private $app;
    
    public function __construct(Application $app) {
        $this->app=$app;
    }
    
    
    public function render($viewPath,array $data=[]){
        return new View($this->app->file, $viewPath, $data);
    }
    
}
