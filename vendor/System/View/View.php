<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace System\View;
use System\File;
/**
 * Description of View
 *
 * @author youssef
 */
class View implements ViewInterface {
    
    private $file;
    
    private $viewPath;

    private $data=[];
    
    private $output;

    public function __construct(File $file,$viewPath,array $data) {
        $this->file=$file;
        $this->preparePath($viewPath);
        $this->data=$data;
    }
    
    private function preparePath($viewPath){
        $viewPath='App/Views/'.$viewPath.'.php';
        if(!$this->viewFileExists($viewPath)){
            die($viewPath.' doesnot exist');
        }
        $this->viewPath=  $this->file->to($viewPath);
        
    }
    
    private function viewFileExists($viewPath){
        return $this->file->exists($viewPath);
    }

    public function __toString() {
        return $this->getOutput();
    }

    public function getOutput() {
        if(is_null($this->output)){
        ob_start();
        extract($this->data);
        require $this->viewPath;
        $this->output=  ob_get_clean();
        }
        return $this->output;
    }


}
