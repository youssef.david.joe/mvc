<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace System;

/**
 * Description of Loader
 *
 * @author youssef
 */
class Loader {
    private $app;
    private $controllers=[];
    private $models=[];
    
    public function __construct(Application $app) {
        $this->app=$app;
    }
    
    public function action($controller,$method,array $arguments){
        $object=  $this->controller($controller);
        return call_user_func([$object,$method],$arguments);
    }
    
    public function controller($controller){
        $controller=  $this->getControllerName($controller);
        if(!$this->hasController($controller)){
            $this->addController($controller);
        }
        return $this->getController($controller);
    }
    
    private function hasController($controller){
        return array_key_exists( $controller,$this->controllers);
    }
    
    private function addController($mcontroller){
        $object=new $mcontroller($this->app);
        $this->controllers[$mcontroller]=$object;
    }
    
    private function getController($controller){
        return $this->controllers[$controller];
    }
    
    private function getControllerName($controller){
        return str_replace('/', '\\', "App\\Controllers\\".$controller."Controller") ;
    }

    public function model($model){
         $model=  $this->getModelName($model);
        if(!$this->hasModel($model)){
            $this->addModel($model);
        }
        return $this->getModel($model);
    }
    
    private function hasModel($model){
         return array_key_exists( $model,$this->models);
    }
    
    private function addModel($model){
        $object=new $model($this->app);
        $this->models[$model]=$object;
    }
    
    private function getModel($model){
        return $this->models[$model];
    }
    private function getModelName($model){
        return str_replace('/', '\\', "App\\Models\\".$model."Model") ;
    }
}
