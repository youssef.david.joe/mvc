<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace System;

/**
 * Description of Model
 *
 * @author youssef
 */
class Model {
    
    protected $app;
    
    public function __construct(Application $app) {
        $this->app=$app;
    }
    
    public function __get($key) {
        $this->app->get($key);
    }
    
    public function __call($method,$args) {
        return call_user_func([$this->app->DB,$method],$args);
    }
}
