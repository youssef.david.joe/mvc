<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace System;

/**
 * Description of Html
 *
 * @author youssef
 */
class Html {
    
    protected $app;
    
    private $title;
    
    private $description;
    
    private $keywords;


    public function __construct(Application $app) {
        $this->app=$app;
    }
    
    public function setTitle($title) {
        $this->title=$title;
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function setDescription($description) {
        $this->description=$description;
    }
    
    public function getDescription() {
        return $this->description;
    }
    
    public function setKeywords($keywords) {
        $this->keywords=$keywords;
    }
    
    public function getKeywords() {
        return $this->keywords;
    }
    
}
