<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace System;

/**
 * Description of Route
 *
 * @author youssef
 */
class Route {
    private $app;
    private $routes=[];
    public function __construct(Application $app) {
        $this->app=$app;
    }
    
    public function Add($url,$action,$method='GET'){
        $route=[
            'url'=>$url,
            'action'=>  $this->getAction($action),
            'pattern'=>  $this->generatePattern($url),
            'method'=>$method
        ];
        
        array_push($this->routes, $route);
    }
    
    
    private function generatePattern($url){
        $pattern='#^';
        
        $pattern.=str_replace([':text',':id'], ['([a-zA-Z0-9-]+)','(\d+)'], $url);
        $pattern.='$#';
        return $pattern;
    }
    
    private function getAction($action){
        $action=  str_replace('/', '\\', $action);
        return strpos($action, '@')!==FALSE?$action:$action.'@index';
        
    }
    
    public function getproperRoute(){
        foreach ($this->routes as $route){
            if($this->isMatching($route['pattern']) ){
                //echo $route['pattern'];
                $arguments=  $this->getArgumentFrom($route['pattern']);
                list($controller,$method)=  explode('@', $route['action']);
                return [$controller,$method,$arguments];
            }
        }
    }
    
    
    
    private function isMatching($pattern){
        return preg_match($pattern, $this->app->Request->url());
    }
    
    private function getArgumentFrom($pattern){
        preg_match($pattern, $this->app->Request->url(),$matches);
        array_shift($matches);
        return $matches;
    }
}
