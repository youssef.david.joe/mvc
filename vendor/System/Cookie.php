<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace System;

/**
 * Description of Cookie
 *
 * @author youssef
 */
class Cookie {
    private $app;
    public function __construct(Application $app) {
        $this->app=$app;
    }
    
     public function start(){
        ini_set('Session.use_only_cookies', 1);
        if(!session_id()){
            session_start();
        }
    }
    
    public function set($key,$value,$minutes=1800){
       //$_COOKIE[$key]=$value;
       setcookie($key,$value,  time()+($minutes*3600),'','',FALSE,TRUE);
    }
    
    public function get($key,$default=null){
        return $this->has($key)? $_COOKIE[$key]:$default;
    }
    
    
    public function has($key){
        return array_key_exists($key, $_COOKIE);
    }
    
    public function remove($key){
        setcookie($key,null,-1);
        unset($_COOKIE[$key]);
    }
    
    public function all(){
        return $_COOKIE;
    }
    
    public function destroy(){
        foreach (array_keys($this->all())as $key){
            $this->remove($key);
        }
        unset($_COOKIE);
    }
    
   
}
