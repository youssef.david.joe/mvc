<?php

namespace System;

class File{  
    const DS=DIRECTORY_SEPARATOR;
    /*
     * @var:strign 
     */
    private $root;
    
    
    public function __construct($root){
        $this->root=$root;
    }
    
    public function exists($file){
        return file_exists($this->to($file));
    }
    
    public function toVendor($class){
       // echo $class;
        return $this->to('vendor/'.$class);
    }

    public function to($path){
      //echo static::DS;
        return $this->root .static::DS . str_replace(['/', '\\'], static::DS,$path);
    }
    public function requir($file){
       return require $this->to($file);
    }
    
}