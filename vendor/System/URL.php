<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace System;
use System\Application;
class URL {
    protected $app;
    
    public function __construct(Application $app) {
        $this->app=$app;
    }
    
    public function link($path){
        return $this->app->Request->baseUrl().  trim($path,'/');
    }
    
    public function redirectTo($path) {
        header('location:'.$this->link($path));
        exit();
    }
    
    
}
