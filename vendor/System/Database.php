<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace System;

use PDO;
use PDOException;

class Database {
    
    private $app;
    
    private static $connection;
    
    private $table;
    
    private $data=[];
    
    private $bindings=[];

    private $lastId;
    
    private $wheres=[];
    
    private $selects=[];
    
    private $joins=[];
    
    private $limit=[];
    
    private $offset;
    
    private $orderBy=[];
    
    private $rows=0;






    public function __construct(Application $app) {
        $this->app=$app;
        
        if(!$this->isConnected()){
            $this->connect();
        }
    }
    
    
    private function isConnected(){
        return static::$connection instanceof PDO;
    }
    
    private function connect(){
        $dataConnection=  $this->app->file->requir('config.php');
        //pre($dataConnection);
        try {
             static::$connection=new PDO('mysql:host='.$dataConnection['server'].';dbname='.$dataConnection['dbname'],$dataConnection['dbuser'],$dataConnection['dbpass']);
             static::$connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
             static::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
             static::$connection->exec('SET NAMES utf8');

        } catch (PDOException $pdoex) {
            die($pdoex->getMessage());
        }
       // echo $this->isConnected();
    }
    
    
    public function connection(){
        return static::$connection;
    }
    
    
    public function select($select){
        $this->selects[]=$select;
        return $this;
    }

    
    public function join($join){
        $this->joins[]=$join;
        return $this;
    }
    
    
    public function limit($limit,$offset=0){
        $this->limit=$limit;
        $this->offset=$offset;
        return $this;
    }
    
    
    public function orderBy($orderBy,$sort){
        $this->orderBy=[$orderBy,$sort];
        return $this;
    }
    
    public function fetch($table=NULL){
        if($table!=$table){
            $this->table=$table;
        }
        $sql=  $this->fetchStatment();
        $result=$this->query($sql,  $this->bindings)->fetch();
        $this->reset();
        return $result;
    }
    
    public function fetchALL($table=NULL){
        if($table!=$table){
            $this->table=$table;
        }
        $sql=  $this->fetchStatment();
        $query=$this->query($sql,  $this->bindings);
        $results=$query->fetchALL();
        $this->rows=$query->rowCount();
        $this->reset();
        return $results;
    }
    
    public function getRowCount(){
        return $this->rows;
    }

    private function fetchStatment(){
        $sql='SELECT ';
        $this->selects?$sql.=implode(',', $this->selects):$sql.='*';
        $sql.=' FROM '.$this->table.' ';       
        $this->joins?$sql.=implode(' ', $this->joins):NULL;
        $this->wheres?$sql.=' WHERE '.implode(' ', $this->wheres).' ':NULL; 
        $this->limit?$sql.=' LIMIT '.$this->limit:NULL;       
        $this->offset?$sql.='OFFSET '.  $this->offset:NULL;   
        $this->orderBy?$sql.=' ORDER BY '.  implode(' ', $this->orderBy):NULL;
        return $sql;
    }

    
    public function tabel($table){
        $this->table=$table;
        return $this;
    }
    
    
    public function from($table){
        return $this->tabel($table);
    }
    
    public function data($key,$value=NULL){
        if(is_array($key)){
            $this->data=  array_merge($this->data,$key);
            $this->addToBindings($key);
        }  else {
            $this->data[$key]=$value;
        }
        return $this;
    }
    
    public function insert($table=null){
        if($table!=NULL){
            $this->table($table);
        }
        $sql='insert into '.$this->table.' set ';
        $sql.=$this->setFields();
        $this->query($sql,  $this->bindings);
        $this->lastId=  $this->connection()->lastInertId();
    }
    
    public function query(...$bindings){
        $sql=  array_shift($bindings);
        if(count($bindings)==1 and is_array($bindings[0])){
            $bindings=$bindings[0];
        }
        
        try {
            $query= $this->connection()->prepare($sql);
            foreach ($bindings as $key =>$value){
                $query->bindValue($key+1,$value);
            }
            $query->execute();
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    private function addToBindings($value){
        
        if (is_array($value)) {
            $this->bindings = array_merge($this->bindings, array_values($value));
        } else {
            $this->bindings[] = _e($value);
        }
    }
    
    public function lastId(){
        return $this->lastId;
    }
    
    public function where(...$bindings){
       $sql=  array_shift($bindings);
       $this->addToBindings($bindings);
       $this->wheres=$sql;
        return $this;
    }
    
    
    public function update($table=NULL){
        if($table!=NULL){
            $this->table=$table;
        }
        $sql='UPDATE '.$this->table.' SET ';
        $sql.=$this->setFields();
       
        $this->wheres?$sql.=' WHERE '.implode(' ', $this->wheres):NULL; 
        $this->query($sql,  $this->bindings);
        return $this;
    }
    
    public function delete($table=NULL){
        if($table!=NULL){
            $this->table=$table;
        }
        $sql='DELETE FROM '.$this->table.' ';
    
        $this->wheres?$sql.=' WHERE '.implode(' ', $this->wheres):NULL; 
        $this->query($sql,  $this->bindings);
        return $this;
    }
    
    private function setFields(){
        $sql='';
         foreach (array_keys($this->data) as $key){
            $sql.='`'.$key.'` = ? , ';
        }
        $sql=  rtrim($sql,', ');
        return $sql;
    }
    
    private function reset(){
        $this->data=[];
        $this->joins=[];
        $this->bindings=[];
        $this->wheres=[];
        $this->limit=NULL;
        $this->offset=NULL;
        $this->orderBy=[];
        $this->selects=[];
    }
    

}
