<?php
namespace System;


class Application{
    
    
    private $container=[];
    
    private static $instance;


    private function __construct(File $file) {
        $this->Share('file', $file);
        $this->registerClasses();
        $this->loadHelper();
        static::$instance=  $this;
    }
    
    public static function getInstance($file=NULL){
        if(is_null(static::$instance)){
            static::$instance=new static($file);
        }
        return static::$instance;
    }

    

    public function run(){
        $this->Session->start();
        $this->Request->prepareURL();
        $this->file->requir('App/index.php');
        list($controller,$method,$arguments)=$this->Route->getproperRoute();
        $output= (string)  $this->Load->action($controller,$method,$arguments);
        $this->Response->setOutput($output);
        $this->Response->send();
    }

        public function registerClasses(){
        spl_autoload_register([$this,'load']);
    }
    
    
    
    public function load($class){
       
        
        if(strpos($class,'App')===0){
             $file=  $class.'.php';
        }else{
            $file=  'vendor/'.$class.'.php';          
        }
         if($this->file->exists($file)){   
                $this->file->requir($file);
            }
        
    }
    

    public function get($key){
        if(!$this->isSharing($key)){
            if($this->isCoreAlias($key)){
                $this->Share($key, $this->createNewObject($key));
            }  else {
                die("not found <b>".$key."</b> in Application");   
            }
        }
        return $this->container[$key];
    }
    
    private function createNewObject($key){
        $arr=  $this->coreClasses();
        $obj=$arr[$key];
       return new $obj($this);
    }

        private function isCoreAlias($key){
        $arr=$this->coreClasses();
        return isset($arr[$key]);
    }

    public function isSharing($key){
        return isset($this->container[$key]);
    }

        public function __get($key) {
        return $this->get($key);
    }

    public function Share($key,$value){
        $this->container[$key]=$value;
    }
    
    private function coreClasses(){
        return [
            'Request'   =>    'System\\Http\\Request',
            'Response'  =>    'System\\Http\\Response',
            'Session'   =>    'System\\Session',
            'Route'     =>    'System\\Route',      
            'Cookie'    =>    'System\\Cookie',
            'Load'      =>    'System\\Loader',
            'HTML'      =>    'System\\Html',
            'DB'        =>    'System\\Database',
            'View'      =>    'System\\View\\ViewFactory'
        ];
    }
    
    
    private function loadHelper(){
        $this->file->requir('vendor/helpers.php');
    }
}
